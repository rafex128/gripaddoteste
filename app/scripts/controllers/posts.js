'use strict';


app.controller('PostsCtrl', function ($scope, fApi, $route, $rootScope) {
	$scope.loadMorePos = 1;
	$scope.controller = $route.current.controller;
	$scope.user = $rootScope.currentUser;
	$scope.userPosts = [];
	$scope.aux = [];
	fApi.getPost(function (data){
		$scope.aux = data;
		for (var i = 0; i < data.length; i++) {
			if (data[i].userId === parseInt($route.current.params.id)) {
				$scope.userPosts[$scope.userPosts.length] = data[i];
			};
		};
	});
	$scope.loadMore = function(id){
		$scope.loadMorePos = id;
		if (id===1) $scope.userPosts[0] = $scope.aux[0];
		else $scope.userPosts[0] = $scope.aux[id];
		$scope.userPosts[1] = $scope.aux[id+1];
	};
});
