'use strict';

app.controller('UsersCtrl', function ($scope, fApi, $rootScope, $route) {
	$scope.controller = $route.current.controller;
	$scope.users = [];
	$scope.user = [];
	fApi.getUsers(function (data){
		$scope.users = data;
		$scope.user = data[0];
		$rootScope.currentUser = $scope.user;
	});

	$scope.next = function(idNext){
		if (!$scope.users[idNext+1]) $scope.user = $scope.users[0];
		else $scope.user = $scope.users[idNext];

		$rootScope.currentUser = $scope.user;
	};

	$scope.previous = function(idPrevious){
		if (!$scope.users[idPrevious-2]) $scope.user = $scope.users[$scope.users.length-1];
		else $scope.user = $scope.users[idPrevious-2];	

		$rootScope.currentUser = $scope.user;
	};	
  });
