'use strict';

/**
 * @ngdoc overview
 * @name grippadoApp
 * @description
 * # grippadoApp
 *
 * Main module of the application.
 */
 var app = angular.module('grippadoApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch'
  ]);

//Aqui estou criando uma constante para api, assim nao precisamos ficar reescrevendo a api toda vez q for criado uma factory
var api = {
  apiUrl: "http://jsonplaceholder.typicode.com/",
  path: function (method) {
    return this.apiUrl+method;
  }
};

var view = {
  folder: 'views/',
  extension: '.html',
  path: function (name) {
    return this.folder + name + this.extension;
  } 
};

app.config(function ($routeProvider) {
  $routeProvider
  .when('/users', {
    templateUrl: view.path('users'),
    controller: 'UsersCtrl'
  })
  .when('/posts/:id', {
    templateUrl: view.path('posts'),
    controller: 'PostsCtrl'
  })
  .otherwise({
    redirectTo: '/users'
  });
});

app.factory("fApi", function ($resource){
  return $resource("", {}, {
    getUsers: {
      url: api.path("users"),
      method: "GET",
      isArray: true
    },
    getPost: {
      url: api.path("posts"),
      method: "GET",
      isArray: true
    }
  })
});